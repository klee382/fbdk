/* Copyright (c)2018 Rockwell Automation. All rights reserved. */
package fb.rt.template;
import fb.datatype.*;
import fb.rt.*;
import fb.rt.events.*;
/** FUNCTION_BLOCK Composite
  * @author JHC
  * @version 20181018/JHC
  */
public class Composite extends FBInstance
{
/** Input event qualifier */
  public BOOL CB2 = new BOOL();
/** VAR CB6 */
  public BOOL CB6 = new BOOL();
/** VAR CB7 */
  public BOOL CB7 = new BOOL();
/** Output event qualifier */
  public BOOL Block2 = new BOOL();
/** VAR Block6 */
  public BOOL Block6 = new BOOL();
/** Initialization Request */
 public EventOutput INIT = new EventOutput();
/** Normal Execution Request */
 public EventOutput REQ = new EventOutput();
/** Initialization Confirm */
 public EventOutput INITO = new EventOutput();
/** Execution Confirmation */
 public EventOutput CNF = new EventOutput();
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
*/
  public EventServer eiNamed(String s){
    if("INIT".equals(s)) return INIT;
    if("REQ".equals(s)) return REQ;
    return super.eiNamed(s);}
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
*/
  public EventOutput eoNamed(String s){
    if("INITO".equals(s)) return INITO;
    if("CNF".equals(s)) return CNF;
    return super.eoNamed(s);}
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
* @throws FBRManagementException {@inheritDoc}
*/
  public ANY ivNamed(String s) throws FBRManagementException{
    if("CB2".equals(s)) return CB2;
    if("CB6".equals(s)) return CB6;
    if("CB7".equals(s)) return CB7;
    return super.ivNamed(s);}
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
* @throws FBRManagementException {@inheritDoc}
*/
  public ANY ovNamed(String s) throws FBRManagementException{
    if("Block2".equals(s)) return Block2;
    if("Block6".equals(s)) return Block6;
    return super.ovNamed(s);}
/** {@inheritDoc}
* @param ivName {@inheritDoc}
* @param newIV {@inheritDoc}
* @throws FBRManagementException {@inheritDoc} */
  public void connectIV(String ivName, ANY newIV)
    throws FBRManagementException{
    if("CB2".equals(ivName)) connect_CB2((BOOL)newIV);
    else if("CB6".equals(ivName)) connect_CB6((BOOL)newIV);
    else if("CB7".equals(ivName)) connect_CB7((BOOL)newIV);
    else super.connectIV(ivName, newIV);
    }
/** Connect the given variable to the input variable CB2
  * @param newIV The variable to connect
  * @throws FBRManagementException An internal connection failed.
 */
  public void connect_CB2(BOOL newIV) throws FBRManagementException{
    CB2 = newIV;
    Priority.connectIVNoException("Block2",CB2);
    }
/** Connect the given variable to the input variable CB6
  * @param newIV The variable to connect
  * @throws FBRManagementException An internal connection failed.
 */
  public void connect_CB6(BOOL newIV) throws FBRManagementException{
    CB6 = newIV;
    Priority.connectIVNoException("Block6",CB6);
    }
/** Connect the given variable to the input variable CB7
  * @param newIV The variable to connect
  * @throws FBRManagementException An internal connection failed.
 */
  public void connect_CB7(BOOL newIV) throws FBRManagementException{
    CB7 = newIV;
    Distributor.connectIVNoException("PE12",CB7);
    }
/** FB Distributor */
  protected TokenTransaction Distributor = new TokenTransaction() ;
/** FB Priority */
  protected TokenGiver Priority = new TokenGiver() ;
/** The default constructor. */
public Composite(){
    super();
    INIT.connectTo(Distributor.INIT);
    REQ.connectTo(Distributor.REQ);
    Distributor.INITO.connectTo(Priority.INIT);
    Distributor.CNF.connectTo(Priority.REQ);
    Distributor.connectIVNoException("Token",Priority.ovNamedNoException("TokenOut"));
    Priority.connectIVNoException("Token",Distributor.ovNamedNoException("Allow"));
    Distributor.connectIVNoException("PE12",CB7);
    Priority.connectIVNoException("Block6",CB6);
    Priority.connectIVNoException("Block2",CB2);
    Block2 = (BOOL)Priority.ovNamedNoException("Stop2");
    Block6 = (BOOL)Priority.ovNamedNoException("Stop6");
  }
/** {@inheritDoc}
 * @param fbName {@inheritDoc}
 * @param r {@inheritDoc}
 * @throws FBRManagementException {@inheritDoc} */
  public void initialize(String fbName, Resource r)
  throws FBRManagementException{
    super.initialize(fbName,r);
    Distributor.initialize("Distributor",r);
    Priority.initialize("Priority",r);
}
/** Start the FB instances. */
public void start( ){
  Distributor.start();
  Priority.start();
}
/** Stop the FB instances. */
public void stop( ){
  Distributor.stop();
  Priority.stop();
}
}
