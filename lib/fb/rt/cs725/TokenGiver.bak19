/* Copyright (c)2018 Rockwell Automation. All rights reserved. */
package fb.rt.cs725;
import fb.datatype.*;
import fb.rt.*;
import fb.rt.events.*;
/** FUNCTION_BLOCK TokenGiver
  * @author JHC
  * @version 20181018/JHC
  */
public class TokenGiver extends FBInstance
{
/** Input event qualifier */
  public BOOL Block2 = new BOOL();
/** VAR Block6 */
  public BOOL Block6 = new BOOL();
/** VAR TokenReturn */
  public BOOL TokenReturn = new BOOL();
/** VAR Stop2 */
  public BOOL Stop2 = new BOOL();
/** VAR Stop6 */
  public BOOL Stop6 = new BOOL();
/** VAR TokenOut */
  public BOOL TokenOut = new BOOL();
/** VAR Holding */
  public BOOL Holding = new BOOL();
/** Initialization Request */
 public EventServer INIT = new EventInput(this);
/** Normal Execution Request */
 public EventServer REQ = new EventInput(this);
/** Initialization Confirm */
 public EventOutput INITO = new EventOutput();
/** Execution Confirmation */
 public EventOutput CNF = new EventOutput();
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
*/
  public EventServer eiNamed(String s){
    if("INIT".equals(s)) return INIT;
    if("REQ".equals(s)) return REQ;
    return super.eiNamed(s);}
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
*/
  public EventOutput eoNamed(String s){
    if("INITO".equals(s)) return INITO;
    if("CNF".equals(s)) return CNF;
    return super.eoNamed(s);}
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
* @throws FBRManagementException {@inheritDoc}
*/
  public ANY ivNamed(String s) throws FBRManagementException{
    if("Block2".equals(s)) return Block2;
    if("Block6".equals(s)) return Block6;
    if("TokenReturn".equals(s)) return TokenReturn;
    return super.ivNamed(s);}
/** {@inheritDoc}
* @param s {@inheritDoc}
* @return {@inheritDoc}
* @throws FBRManagementException {@inheritDoc}
*/
  public ANY ovNamed(String s) throws FBRManagementException{
    if("Stop2".equals(s)) return Stop2;
    if("Stop6".equals(s)) return Stop6;
    if("TokenOut".equals(s)) return TokenOut;
    return super.ovNamed(s);}
/** {@inheritDoc}
* @param ivName {@inheritDoc}
* @param newIV {@inheritDoc}
* @throws FBRManagementException {@inheritDoc} */
  public void connectIV(String ivName, ANY newIV)
    throws FBRManagementException{
    if("Block2".equals(ivName)) connect_Block2((BOOL)newIV);
    else if("Block6".equals(ivName)) connect_Block6((BOOL)newIV);
    else if("TokenReturn".equals(ivName)) connect_TokenReturn((BOOL)newIV);
    else super.connectIV(ivName, newIV);
    }
/** Connect the given variable to the input variable Block2
  * @param newIV The variable to connect
 */
  public void connect_Block2(BOOL newIV){
    Block2 = newIV;
    }
/** Connect the given variable to the input variable Block6
  * @param newIV The variable to connect
 */
  public void connect_Block6(BOOL newIV){
    Block6 = newIV;
    }
/** Connect the given variable to the input variable TokenReturn
  * @param newIV The variable to connect
 */
  public void connect_TokenReturn(BOOL newIV){
    TokenReturn = newIV;
    }
private static final int index_START = 0;
private void state_START(){
  eccState = index_START;
}
private static final int index_INIT = 1;
private void state_INIT(){
  eccState = index_INIT;
  alg_INIT();
  INITO.serviceEvent(this);
state_START();
}
private static final int index_REQ = 2;
private void state_REQ(){
  eccState = index_REQ;
  alg_REQ();
  CNF.serviceEvent(this);
state_START();
}
/** The default constructor. */
public TokenGiver(){
    super();
  }
/** {@inheritDoc}
* @param e {@inheritDoc} */
  public void serviceEvent(EventServer e){
    if (e == INIT) service_INIT();
    else if (e == REQ) service_REQ();
  }
/** Services the INIT event. */
  public void service_INIT(){
    if ((eccState == index_START)) state_INIT();
  }
/** Services the REQ event. */
  public void service_REQ(){
    if ((eccState == index_START)) state_REQ();
  }
  /** ALGORITHM INIT IN ST*/
public void alg_INIT(){
}
  /** ALGORITHM REQ IN Java*/
public void alg_REQ(){
if(Block6.value){
Holding.value=true;
Stop2.value=true;
Stop6.value=false;
} else if(Block2.value) {
Holding.value=true;
Stop2.value=false;
Stop6.value=true;
} else {
Holding.value=false;
Stop2.value=false;
Stop6.value=false;
}

}
}
